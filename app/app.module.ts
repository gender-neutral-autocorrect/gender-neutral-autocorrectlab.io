import './extensions/rxjs-extensions';

import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { FormsModule }          from '@angular/forms';
import { HttpModule }           from '@angular/http';

import { routing }              from './app.routing';

import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { WordService }          from './services/word.service';
import { InMemoryDatabaseService }  from './services/in-memory-database.service';

import { AppComponent }         from './components/app.component';
import { AboutComponent }       from './components/about/about.component';
import { WordDetailComponent }  from './components/word-detail/word-detail.component';
import { WordsComponent }       from './components/words/words.component';
import { WordSearchComponent }  from './components/word-search/word-search.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDatabaseService),
    routing
  ],
  declarations: [
    AppComponent,
    AboutComponent,
    WordsComponent,
    WordDetailComponent,
    WordSearchComponent
  ],
  providers: [
    WordService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
