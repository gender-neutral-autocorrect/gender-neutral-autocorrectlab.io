import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WordsComponent }      from './components/words/words.component';
import { WordDetailComponent }  from './components/word-detail/word-detail.component';
import { AboutComponent }  from './components/about/about.component';

const appRoutes: Routes = [
  {
    path: '',
    component: AboutComponent
  },
  {
    path: 'detail/:is',
    component: WordDetailComponent
  },
  {
    path: ':extras',
    component: AboutComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
