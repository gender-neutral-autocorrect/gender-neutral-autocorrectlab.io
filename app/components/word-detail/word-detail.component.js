"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var word_service_1 = require('../../services/word.service');
var word_1 = require('../../classes/word');
var WordDetailComponent = (function () {
    function WordDetailComponent(wordService, route) {
        this.wordService = wordService;
        this.route = route;
    }
    WordDetailComponent.prototype.goBack = function () {
        window.history.back();
    };
    WordDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var is = params['is'];
            _this.word = _this.wordService.getWord(is);
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', word_1.Word)
    ], WordDetailComponent.prototype, "word", void 0);
    WordDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'selected-word-detail',
            templateUrl: 'word-detail.component.html'
        }), 
        __metadata('design:paramtypes', [word_service_1.WordService, router_1.ActivatedRoute])
    ], WordDetailComponent);
    return WordDetailComponent;
}());
exports.WordDetailComponent = WordDetailComponent;
//# sourceMappingURL=word-detail.component.js.map