import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { WordService } from '../../services/word.service';

import { Word } from '../../classes/word';

@Component({
  moduleId: module.id,
  selector: 'selected-word-detail',
  templateUrl: 'word-detail.component.html'
})

export class WordDetailComponent implements OnInit {
  @Input() word: Word;

  goBack(): void {
    window.history.back();
  }

  constructor(
    private wordService: WordService,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let is = params['is'];
      this.word = this.wordService.getWord(is);
    });
  }
}
