import { Component, OnInit, ApplicationRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WordService } from '../services/word.service';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  template: `
    <a href="/"><img src="img/Logo---GNA---borderless.png" style="max-width: 325px; width: 100%; margin-bottom: 20px;" alt="{{title}}" /></a>
    <h2>
      Use your smartphone's autocorrect capabilities to combat gender bias. Add
      autocorrects for the words below.
      <a href="http://lmgtfy.com/?q=how+to+add+custom+iphone+autocorrects">Learn how to add custom phone autocorrects.</a>
    </h2>
    <h3>
      Hint: you need to add autocorrects 1-by-1, but it only takes a few minutes to add them all.
      Do it now and you'll never need to do it again.
    </h3>
    <nav>
      <a (click)="toggleReplacement('neutral')" class="{{activatedLink['neutral']}}">Neutralizer</a>
      <a (click)="toggleReplacement('female')" class="{{activatedLink['female']}}">Femanizer</a>
      <a (click)="toggleReplacement('oops')" class="{{activatedLink['oops']}}">Habit Breaker</a>
      <div style='margin-top: 20px;'>
        <button (click)="toggleDirtyWords()" style="margin-right: 20px;">{{dirtyText}}</button>
        <button (click)="toggleMoreWords()">{{moreText}}</button>
      </div>
    </nav>
    <router-outlet></router-outlet>
    <footer style="clear:both;"><p><a href="https://gitlab.com/gender-neutral-autocorrect/gender-neutral-autocorrect.gitlab.io">This website is open source. Learn more and contribute on Gitlab.</a></p></footer>
  `
})

export class AppComponent {
  title = 'Gender Neutral Autocorrect';
  activatedLink: any = {
    'neutral': '',
    'female': '',
    'oops': ''
  };
  params = {
    isClean: this.wordService.getConfig()['isClean'],
    showAll: this.wordService.getConfig()['showAll'],
    replaceWith: this.wordService.getConfig()['replaceWith']
  };
  dirtyText: string = 'Hiding Dirty Words';
  moreText: string = 'Hiding Less Common Words';

  constructor(
    private wordService: WordService,
    private route: ActivatedRoute,
    private router: Router,
    private detector: ApplicationRef) {
  }

  ngOnInit(): void {
    this.wordService.setOption('bias', 'male');
    this.wordService.setOption('replaceWith', 'neutral');
    this.wordService.setOption('isClean', 'true');
    this.wordService.setOption('showAll', 'false');
    this.activatedLink[this.wordService.getConfig()['replaceWith']] = 'active';
    this.params = {
      isClean: this.wordService.getConfig()['isClean'],
      showAll: this.wordService.getConfig()['showAll'],
      replaceWith: this.wordService.getConfig()['replaceWith']
    };
    this.route.queryParams.forEach(params => {
      if (params['replaceWith']) {
        this.params['replaceWith'] = params['replaceWith'];
        this.activatedLinkChanger(params['replaceWith']);
      }
      if (params['isClean'] === 'false') {
        this.params['isClean'] = params['isClean'];
        this.dirtyText = 'Showing Dirty Words';
      }
      if (params['showAll'] === 'true') {
        this.params['showAll'] = params['showAll'];
        this.moreText = 'Showing Less Common Words';
      }
    });
  }

// , skipLocationChange: true
  toggleReplacement(replacement: string): void {
    if (replacement === 'neutral') {
      this.params['replaceWith'] = replacement;
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
      this.activatedLinkChanger('neutral');
    }
    else if (replacement === 'female') {
      this.params['replaceWith'] = replacement;
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
      this.activatedLinkChanger('female');
    }
    else {
      this.params['replaceWith'] = replacement;
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
      this.activatedLinkChanger('oops');
    }
  }

  activatedLinkChanger(link: string) {
    this.activatedLink['neutral'] = '';
    this.activatedLink['female'] = '';
    this.activatedLink['oops'] = '';
    this.activatedLink[link] = 'active';
  }

  toggleDirtyWords(): void {
    if (this.wordService.getConfig()['isClean'] === 'false') {
      this.wordService.setOption('isClean', 'true');
      this.params['isClean'] = 'true';
      this.dirtyText = 'Hiding Dirty Words';
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
    }
    else {
      this.wordService.setOption('isClean', 'false');
      this.params['isClean'] = 'false';
      this.dirtyText = 'Showing Dirty Words';
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
    }
  }

  toggleMoreWords(): void {
    if (this.wordService.getConfig()['showAll'] === 'true') {
      this.wordService.setOption('showAll', 'false');
      this.params['showAll'] = 'false';
      this.moreText = 'Hiding Less Common Words';
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
    }
    else {
      this.wordService.setOption('showAll', 'true');
      this.params['showAll'] = 'true';
      this.moreText = 'Showing Less Common Words';
      this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
    }
  }
}
