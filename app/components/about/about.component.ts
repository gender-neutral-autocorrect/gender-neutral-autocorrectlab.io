import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Word } from '../../classes/word';
import { WORDS } from '../../services/words';
import { WordService } from '../../services/word.service';

@Component({
  moduleId: module.id,
  selector: 'my-about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css']
})

export class AboutComponent implements OnInit {

  words: Word[] = [];
  wordCount: number = 0;
  params = {
    bias: 'male',
    replaceWith: 'neutral',
    isClean: 'true',
    showAll: 'false'
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private wordService: WordService,
    private detector: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.params['bias'] = this.wordService.getConfig()['bias'];

    this.route.queryParams.forEach(params => {
      if (params['replaceWith']) {
        this.params['replaceWith'] = params['replaceWith'];
      }
      if (params['isClean']) {
        this.params['isClean'] = params['isClean'];
      }
      if (params['showAll']) {
        this.params['showAll'] = params['showAll'];
      }
    });

    this.words = this.wordService.getWords(this.params);
    this.wordCount = this.words.length;

    this.route.queryParams.subscribe(params => {
      this.paramsChanged(params);
    });
  }

  paramsChanged(params) {
    if (params['replaceWith']) {
      this.params['replaceWith'] = params['replaceWith'];
    }
    if (params['isClean']) {
      this.params['isClean'] = params['isClean'];
    }
    if (params['showAll']) {
      this.params['showAll'] = params['showAll'];
    }

    this.words = this.wordService.getWords(this.params);
    this.wordCount = this.words.length;
  }

  gotoDetail(word: Word): void {
    let link = ['/detail', word.is];
    this.router.navigate(link);
  }

  printReplacement(word: Word): string {
    if (this.wordService.getConfig()['replaceWith'] === 'neutral') {
      return word.replaceWith.neutral;
    } else if (this.wordService.getConfig()['replaceWith'] === 'female') {
      return word.replaceWith.female;
    }
    else {
      return "OOPS!";
    }
  }

}
