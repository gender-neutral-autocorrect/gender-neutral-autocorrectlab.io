"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var word_service_1 = require('../../services/word.service');
var AboutComponent = (function () {
    function AboutComponent(router, route, wordService, detector) {
        this.router = router;
        this.route = route;
        this.wordService = wordService;
        this.detector = detector;
        this.words = [];
        this.wordCount = 0;
        this.params = {
            bias: 'male',
            replaceWith: 'neutral',
            isClean: 'true',
            showAll: 'false'
        };
    }
    AboutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.params['bias'] = this.wordService.getConfig()['bias'];
        this.route.queryParams.forEach(function (params) {
            if (params['replaceWith']) {
                _this.params['replaceWith'] = params['replaceWith'];
            }
            if (params['isClean']) {
                _this.params['isClean'] = params['isClean'];
            }
            if (params['showAll']) {
                _this.params['showAll'] = params['showAll'];
            }
        });
        this.words = this.wordService.getWords(this.params);
        this.wordCount = this.words.length;
        this.route.queryParams.subscribe(function (params) {
            _this.paramsChanged(params);
        });
    };
    AboutComponent.prototype.paramsChanged = function (params) {
        if (params['replaceWith']) {
            this.params['replaceWith'] = params['replaceWith'];
        }
        if (params['isClean']) {
            this.params['isClean'] = params['isClean'];
        }
        if (params['showAll']) {
            this.params['showAll'] = params['showAll'];
        }
        this.words = this.wordService.getWords(this.params);
        this.wordCount = this.words.length;
    };
    AboutComponent.prototype.gotoDetail = function (word) {
        var link = ['/detail', word.is];
        this.router.navigate(link);
    };
    AboutComponent.prototype.printReplacement = function (word) {
        if (this.wordService.getConfig()['replaceWith'] === 'neutral') {
            return word.replaceWith.neutral;
        }
        else if (this.wordService.getConfig()['replaceWith'] === 'female') {
            return word.replaceWith.female;
        }
        else {
            return "OOPS!";
        }
    };
    AboutComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-about',
            templateUrl: 'about.component.html',
            styleUrls: ['about.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, word_service_1.WordService, core_1.ChangeDetectorRef])
    ], AboutComponent);
    return AboutComponent;
}());
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=about.component.js.map