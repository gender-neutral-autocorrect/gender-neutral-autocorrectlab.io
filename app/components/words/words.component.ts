import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Word } from '../../classes/word';
import { WordService } from '../../services/word.service';

@Component({
  moduleId: module.id,
  selector: 'my-words',
  templateUrl: 'words.component.html',
  styleUrls: ['words.component.css']
})

export class WordsComponent implements OnInit {
  title = 'Gendered Words';
  words: Word[];
  selectedWord: Word;

  constructor(
    private router: Router,
    private wordService: WordService) {
  }

  getWords(): void {
    this.words = this.wordService.getWords();
  }

  ngOnInit(): void {
    this.getWords();
  }

  onSelect(word: Word): void {
    this.selectedWord = word;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedWord.is]);
  }
}
