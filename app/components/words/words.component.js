"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var word_service_1 = require('../../services/word.service');
var WordsComponent = (function () {
    function WordsComponent(router, wordService) {
        this.router = router;
        this.wordService = wordService;
        this.title = 'Gendered Words';
    }
    WordsComponent.prototype.getWords = function () {
        this.words = this.wordService.getWords();
    };
    WordsComponent.prototype.ngOnInit = function () {
        this.getWords();
    };
    WordsComponent.prototype.onSelect = function (word) {
        this.selectedWord = word;
    };
    WordsComponent.prototype.gotoDetail = function () {
        this.router.navigate(['/detail', this.selectedWord.is]);
    };
    WordsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-words',
            templateUrl: 'words.component.html',
            styleUrls: ['words.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, word_service_1.WordService])
    ], WordsComponent);
    return WordsComponent;
}());
exports.WordsComponent = WordsComponent;
//# sourceMappingURL=words.component.js.map