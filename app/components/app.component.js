"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var word_service_1 = require('../services/word.service');
var AppComponent = (function () {
    function AppComponent(wordService, route, router, detector) {
        this.wordService = wordService;
        this.route = route;
        this.router = router;
        this.detector = detector;
        this.title = 'Gender Neutral Autocorrect';
        this.activatedLink = {
            'neutral': '',
            'female': '',
            'oops': ''
        };
        this.params = {
            isClean: this.wordService.getConfig()['isClean'],
            showAll: this.wordService.getConfig()['showAll'],
            replaceWith: this.wordService.getConfig()['replaceWith']
        };
        this.dirtyText = 'Hiding Dirty Words';
        this.moreText = 'Hiding Less Common Words';
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.wordService.setOption('bias', 'male');
        this.wordService.setOption('replaceWith', 'neutral');
        this.wordService.setOption('isClean', 'true');
        this.wordService.setOption('showAll', 'false');
        this.activatedLink[this.wordService.getConfig()['replaceWith']] = 'active';
        this.params = {
            isClean: this.wordService.getConfig()['isClean'],
            showAll: this.wordService.getConfig()['showAll'],
            replaceWith: this.wordService.getConfig()['replaceWith']
        };
        this.route.queryParams.forEach(function (params) {
            if (params['replaceWith']) {
                _this.params['replaceWith'] = params['replaceWith'];
                _this.activatedLinkChanger(params['replaceWith']);
            }
            if (params['isClean'] === 'false') {
                _this.params['isClean'] = params['isClean'];
                _this.dirtyText = 'Showing Dirty Words';
            }
            if (params['showAll'] === 'true') {
                _this.params['showAll'] = params['showAll'];
                _this.moreText = 'Showing Less Common Words';
            }
        });
    };
    // , skipLocationChange: true
    AppComponent.prototype.toggleReplacement = function (replacement) {
        if (replacement === 'neutral') {
            this.params['replaceWith'] = replacement;
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
            this.activatedLinkChanger('neutral');
        }
        else if (replacement === 'female') {
            this.params['replaceWith'] = replacement;
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
            this.activatedLinkChanger('female');
        }
        else {
            this.params['replaceWith'] = replacement;
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
            this.activatedLinkChanger('oops');
        }
    };
    AppComponent.prototype.activatedLinkChanger = function (link) {
        this.activatedLink['neutral'] = '';
        this.activatedLink['female'] = '';
        this.activatedLink['oops'] = '';
        this.activatedLink[link] = 'active';
    };
    AppComponent.prototype.toggleDirtyWords = function () {
        if (this.wordService.getConfig()['isClean'] === 'false') {
            this.wordService.setOption('isClean', 'true');
            this.params['isClean'] = 'true';
            this.dirtyText = 'Hiding Dirty Words';
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
        }
        else {
            this.wordService.setOption('isClean', 'false');
            this.params['isClean'] = 'false';
            this.dirtyText = 'Showing Dirty Words';
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
        }
    };
    AppComponent.prototype.toggleMoreWords = function () {
        if (this.wordService.getConfig()['showAll'] === 'true') {
            this.wordService.setOption('showAll', 'false');
            this.params['showAll'] = 'false';
            this.moreText = 'Hiding Less Common Words';
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
        }
        else {
            this.wordService.setOption('showAll', 'true');
            this.params['showAll'] = 'true';
            this.moreText = 'Showing Less Common Words';
            this.router.navigate([''], { queryParams: this.params, relativeTo: this.route });
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-app',
            template: "\n    <a href=\"/\"><img src=\"img/Logo---GNA---borderless.png\" style=\"max-width: 325px; width: 100%; margin-bottom: 20px;\" alt=\"{{title}}\" /></a>\n    <h2>\n      Use your smartphone's autocorrect capabilities to combat gender bias. Add\n      autocorrects for the words below.\n      <a href=\"http://lmgtfy.com/?q=how+to+add+custom+iphone+autocorrects\">Learn how to add custom phone autocorrects.</a>\n    </h2>\n    <h3>\n      Hint: you need to add autocorrects 1-by-1, but it only takes a few minutes to add them all.\n      Do it now and you'll never need to do it again.\n    </h3>\n    <nav>\n      <a (click)=\"toggleReplacement('neutral')\" class=\"{{activatedLink['neutral']}}\">Neutralizer</a>\n      <a (click)=\"toggleReplacement('female')\" class=\"{{activatedLink['female']}}\">Femanizer</a>\n      <a (click)=\"toggleReplacement('oops')\" class=\"{{activatedLink['oops']}}\">Habit Breaker</a>\n      <div style='margin-top: 20px;'>\n        <button (click)=\"toggleDirtyWords()\" style=\"margin-right: 20px;\">{{dirtyText}}</button>\n        <button (click)=\"toggleMoreWords()\">{{moreText}}</button>\n      </div>\n    </nav>\n    <router-outlet></router-outlet>\n    <footer style=\"clear:both;\"><p><a href=\"https://gitlab.com/gender-neutral-autocorrect/gender-neutral-autocorrect.gitlab.io\">This website is open source. Learn more and contribute on Gitlab.</a></p></footer>\n  "
        }), 
        __metadata('design:paramtypes', [word_service_1.WordService, router_1.ActivatedRoute, router_1.Router, core_1.ApplicationRef])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map