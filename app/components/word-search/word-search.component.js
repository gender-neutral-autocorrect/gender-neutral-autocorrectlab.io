"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var Subject_1 = require('rxjs/Subject');
var word_search_service_1 = require('../../services/word-search.service');
var WordSearchComponent = (function () {
    function WordSearchComponent(wordSearchService, router) {
        this.wordSearchService = wordSearchService;
        this.router = router;
        this.searchTerms = new Subject_1.Subject();
    }
    // Push a search term into the observable stream.
    WordSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    };
    WordSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.words = this.searchTerms
            .debounceTime(300) // wait for 300ms pause in events
            .distinctUntilChanged() // ignore if next search term is same as previous
            .switchMap(function (term) { return term // switch to new observable each time
            ? _this.wordSearchService.search(term)
            : Observable_1.Observable.of([]); })
            .catch(function (error) {
            // TODO: real error handling
            console.log(error);
            return Observable_1.Observable.of([]);
        });
    };
    WordSearchComponent.prototype.gotoDetail = function (word) {
        var link = ['/detail', word.is];
        this.router.navigate(link);
    };
    WordSearchComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'word-search',
            templateUrl: 'word-search.component.html',
            styleUrls: ['word-search.component.css'],
            providers: [word_search_service_1.WordSearchService]
        }), 
        __metadata('design:paramtypes', [word_search_service_1.WordSearchService, router_1.Router])
    ], WordSearchComponent);
    return WordSearchComponent;
}());
exports.WordSearchComponent = WordSearchComponent;
//# sourceMappingURL=word-search.component.js.map