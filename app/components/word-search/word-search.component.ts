import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
import { WordSearchService } from '../../services/word-search.service';

import { Word } from '../../classes/word';

@Component({
  moduleId: module.id,
  selector: 'word-search',
  templateUrl: 'word-search.component.html',
  styleUrls: [ 'word-search.component.css' ],
  providers: [ WordSearchService ]
})

export class WordSearchComponent implements OnInit {

  words: Observable<Word[]>;
  private searchTerms = new Subject<string>();
  constructor(
    private wordSearchService: WordSearchService,
    private router: Router) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.words = this.searchTerms
      .debounceTime(300)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time
        // return the http search observable
        ? this.wordSearchService.search(term)
        // or the observable of empty words if no search term
        : Observable.of<Word[]>([]))
      .catch(error => {
        // TODO: real error handling
        console.log(error);
        return Observable.of<Word[]>([]);
      });
  }

  gotoDetail(word: Word): void {
    let link = ['/detail', word.is];
    this.router.navigate(link);
  }
}
