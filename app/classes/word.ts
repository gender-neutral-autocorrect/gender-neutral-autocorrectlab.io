export class Word {
  id: number;
  is: string;
  bias: string;
  isGender: string;
  isClean: boolean;
  isBasic: boolean;
  speechPart: [ string ];
  singular: string;
  plural: string;
  replaceWith: {
    neutral: string,
    female: string
  };
}
