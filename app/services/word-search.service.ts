import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs';

import { Word }           from '../classes/word';

@Injectable()
export class WordSearchService {

  constructor(private http: Http) {}

  search(term: string): Observable<Word[]> {
    return this.http
               .get(`app/words/?is=${term}`)
               .map((r: Response) => r.json().data as Word[]);
  }
}
