import 'rxjs/add/operator/toPromise';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Word } from '../classes/word';
import { WORDS } from './words';

@Injectable()

export class WordService {

  private config: any = {};
  // private wordsUrl: string = 'app/words';
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  constructor(private http: Http) { }

  setOption(option, value) {
    this.config[option] = value;
  }

  getConfig() {
      return this.config;
  }

  getWord(is: string): Word {
    /*
    return this.http.get(this.wordsUrl)
               .toPromise()
               .then(response => response.json().data as Word[])
               .then(words => words.find(word => word.is === is))
               .catch(this.handleError);
    */
    return this.getWords().find(word => word.is === is);
  }

  getWords(params?: any): Array<Word> {
      if (params['bias']) { this.setOption('bias', params['bias']); }
      if (params['replaceWith']) { this.setOption('replaceWith', params['replaceWith']); }
      if (params['isClean']) { this.setOption('isClean', params['isClean']); }
      if (params['showAll']) { this.setOption('showAll', params['showAll']); }

      /*
      return this.http.get(this.wordsUrl)
                 .toPromise()
                 .then(response => response.json().data as Word[])
                 .then(words => words.filter(word => word.bias === 'male'))
                 .catch(this.handleError);
      */

      // alert("bias: " + this.getConfig()['bias'] + ", replaceWith: " + this.getConfig()['replaceWith'] + ", isClean: " + this.getConfig()['isClean'] + ", showAll: " + this.getConfig()['showAll'] );

      let words = WORDS.filter(word => word.bias === this.getConfig()['bias']);

      if (this.getConfig()['isClean'] === 'true') {
        words = words.filter(word => word.isClean);
      }

      if (this.getConfig()['showAll'] === 'false') {
        words = words.filter(word => word.isBasic);
      }

      return words;
  }
}
