"use strict";
var words_1 = require('./words');
var InMemoryDatabaseService = (function () {
    function InMemoryDatabaseService() {
    }
    InMemoryDatabaseService.prototype.createDb = function () {
        return { WORDS: words_1.WORDS };
    };
    return InMemoryDatabaseService;
}());
exports.InMemoryDatabaseService = InMemoryDatabaseService;
//# sourceMappingURL=in-memory-database.service.js.map