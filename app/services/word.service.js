"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
require('rxjs/add/operator/toPromise');
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var words_1 = require('./words');
var WordService = (function () {
    function WordService(http) {
        this.http = http;
        this.config = {};
    }
    // private wordsUrl: string = 'app/words';
    WordService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    WordService.prototype.setOption = function (option, value) {
        this.config[option] = value;
    };
    WordService.prototype.getConfig = function () {
        return this.config;
    };
    WordService.prototype.getWord = function (is) {
        /*
        return this.http.get(this.wordsUrl)
                   .toPromise()
                   .then(response => response.json().data as Word[])
                   .then(words => words.find(word => word.is === is))
                   .catch(this.handleError);
        */
        return this.getWords().find(function (word) { return word.is === is; });
    };
    WordService.prototype.getWords = function (params) {
        var _this = this;
        if (params['bias']) {
            this.setOption('bias', params['bias']);
        }
        if (params['replaceWith']) {
            this.setOption('replaceWith', params['replaceWith']);
        }
        if (params['isClean']) {
            this.setOption('isClean', params['isClean']);
        }
        if (params['showAll']) {
            this.setOption('showAll', params['showAll']);
        }
        /*
        return this.http.get(this.wordsUrl)
                   .toPromise()
                   .then(response => response.json().data as Word[])
                   .then(words => words.filter(word => word.bias === 'male'))
                   .catch(this.handleError);
        */
        // alert("bias: " + this.getConfig()['bias'] + ", replaceWith: " + this.getConfig()['replaceWith'] + ", isClean: " + this.getConfig()['isClean'] + ", showAll: " + this.getConfig()['showAll'] );
        var words = words_1.WORDS.filter(function (word) { return word.bias === _this.getConfig()['bias']; });
        if (this.getConfig()['isClean'] === 'true') {
            words = words.filter(function (word) { return word.isClean; });
        }
        if (this.getConfig()['showAll'] === 'false') {
            words = words.filter(function (word) { return word.isBasic; });
        }
        return words;
    };
    WordService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], WordService);
    return WordService;
}());
exports.WordService = WordService;
//# sourceMappingURL=word.service.js.map