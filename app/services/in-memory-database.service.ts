import { InMemoryDbService } from 'angular2-in-memory-web-api';
import { WORDS } from './words';

export class InMemoryDatabaseService implements InMemoryDbService {
  createDb() {
    return {WORDS};
  }
}
