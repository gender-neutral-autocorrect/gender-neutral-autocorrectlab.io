"use strict";
var router_1 = require('@angular/router');
var word_detail_component_1 = require('./components/word-detail/word-detail.component');
var about_component_1 = require('./components/about/about.component');
var appRoutes = [
    {
        path: '',
        component: about_component_1.AboutComponent
    },
    {
        path: 'detail/:is',
        component: word_detail_component_1.WordDetailComponent
    },
    {
        path: ':extras',
        component: about_component_1.AboutComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map